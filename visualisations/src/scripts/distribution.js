"use strict";

import * as d3 from "d3";
import {
  toAbbreviateUniversity,
  toRegion,
  toTitle,
  groupBy,
  toVerboseDomain,
} from "./utils.js";

/*********/
/* model */
/*********/
let model;

function initModel(data) {
  const wantedCols = ["expertise", "domain", "university", "city", "count"];
  const lines = data.split("\n").filter(l => l);
  const cols = lines
    .shift()
    .split(",")
    .map(col => col.replace(/(_\w)/, c => c.slice(1).toUpperCase()));
  model = {
    current: 0,
    nVis: 3,
    data: lines
      .map(line =>
        Object.fromEntries(
          line
            .split(",")
            .map((elem, i) => [
              cols[i],
              cols[i] === "count" ? parseInt(elem) : elem,
            ])
            .filter(elem => wantedCols.includes(elem[0])),
        ),
      )
      .map(d => ({ ...d, ...{ domain: toVerboseDomain(d.domain) } })),
    // .filter(d => d.university !== "Bishop university"),
    nodes: null,
    links: null,
    simulation: null,
    colorScale: null,
    sizeScale: null,
  };
  createNodesAndLinks();
}

function createNodesAndLinks() {
  const svg = document.querySelector("#distribution");
  const width = svg.getBoundingClientRect().width;
  const height = svg.getBoundingClientRect().height;

  const expertiseUniversityDomain = groupBy(
    model.data.map(d => ({
      ...d,
      ...{ region: toRegion(d.university) },
    })),
    "expertise",
    "university",
    "domain",
    "region",
  );
  const universityDomain = groupBy(
    expertiseUniversityDomain,
    "university",
    "domain",
  ).map(d => ({
    ...d,
    ...{ expertise: null },
  }));
  const university = [
    ...new Set(expertiseUniversityDomain.map(d => d.university)),
  ].map(d => ({
    expertise: null,
    university: d,
    domain: null,
    count: 1,
    region: null,
  }));
  const regionDomain = groupBy(expertiseUniversityDomain, "domain", "region");
  const regions = [
    ...new Set(expertiseUniversityDomain.map(d => d.region)),
  ].map(d => ({
    expertise: null,
    university: null,
    domain: null,
    count: 1,
    region: d,
  }));

  model.nodes = expertiseUniversityDomain
    .map(d => ({
      ...d,
      ...{ group: "expertise-university-domain" },
    }))
    .concat(
      universityDomain.map(d => ({
        ...d,
        ...{ group: "university-domain" },
      })),
    )
    .concat(
      university.map(d => ({
        ...d,
        ...{ group: "university" },
      })),
    )
    .concat(
      regionDomain.map(d => ({
        ...d,
        ...{ group: "region-domain" },
      })),
    )
    .concat(
      regions.map(d => ({
        ...d,
        ...{ group: "region" },
      })),
    )
    .map((d, i) => ({
      ...d,
      ...{ id: i, x: width / 2, y: height / 2 },
    }));

  model.links = model.nodes
    .filter(node => node.group === "expertise-university-domain")
    .map(node => ({
      source: node.id,
      target: model.nodes.filter(
        d =>
          d.group === "university-domain" &&
          d.university === node.university &&
          d.domain === node.domain,
      )[0].id,
      strength: 1,
      group: "university-domain",
    }))
    .concat(
      model.nodes
        .filter(node => node.group === "university-domain")
        .map(node => ({
          source: node.id,
          target: model.nodes.filter(
            d => d.group === "university" && d.university === node.university,
          )[0].id,
          strength: 1,
          group: "university",
        })),
    )
    .concat(
      model.nodes
        .filter(node => node.group === "region-domain")
        .map(node => ({
          source: node.id,
          target: model.nodes.filter(
            d => d.group === "region" && d.region === node.region,
          )[0].id,
          strength: 1,
          group: "region",
        })),
    );
}

/**********/
/* update */
/**********/

function scrollGraph(next) {
  if (next) {
    if (model.current + 1 === model.nVis) {
      return false;
    } else {
      model.current = model.current + 1;
      updateForceGraph();
      return true;
    }
  } else {
    if (model.current === 0) {
      return false;
    } else {
      model.current = model.current - 1;
      updateForceGraph();
      return true;
    }
  }
}

function updateNodeAndLinks() {
  let nodes;
  let links;
  if (model.current === 0) {
    nodes = model.nodes
      .filter(
        d =>
          d.group === "expertise-university-domain" ||
          d.group === "university-domain" ||
          d.group === "university",
      )
      .map(d =>
        d.group === "expertise-university-domain"
          ? { ...d, ...{ size: d.count, fill: true } }
          : d.group === "university-domain"
          ? { ...d, ...{ size: 1, fill: false } }
          : { ...d, ...{ size: 1, fill: false } },
      );
    links = model.links
      .filter(d => d.group === "university-domain" || d.group === "university")
      .map(d =>
        d.group === "university-domain"
          ? { ...d, ...{ strength: 0.75 } }
          : { ...d, ...{ strength: 1 } },
      );
  } else if (model.current === 1) {
    nodes = model.nodes
      .filter(d => d.group === "university-domain" || d.group === "university")
      .map(d =>
        d.group === "university-domain"
          ? { ...d, ...{ size: d.count, fill: true } }
          : { ...d, ...{ size: 1, fill: false } },
      );
    links = model.links
      .filter(d => d.group === "university")
      .map(d => ({
        ...d,
        ...{
          strength: 0.5,
        },
      }));
  } else if (model.current === 2) {
    nodes = model.nodes
      .filter(d => d.group === "region-domain" || d.group === "region")
      .map(d =>
        d.group === "region-domain"
          ? { ...d, ...{ size: d.count, fill: true } }
          : { ...d, ...{ size: 1, fill: false } },
      );
    links = model.links
      .filter(d => d.group === "region")
      .map(d => ({
        ...d,
        ...{
          strength: 1,
        },
      }));
  }
  return [nodes, links];
}

function updateForceGraph() {
  const svg = d3.select("#distribution");

  const [nodes, links] = updateNodeAndLinks();

  svg.select("#distribution-title foreignObject").html(getTitle());
  svg.select("#distribution-explanation foreignObject").html(getExplanation());

  svg
    .select("#nodes")
    .selectAll("*")
    .remove();
  svg
    .select("#node-texts")
    .selectAll("*")
    .remove();

  updateSimulation();
  updateControls();

  const textElements = svg
    .select("#node-texts")
    .selectAll(".node-text")
    .data(
      model.current === 2
        ? nodes.filter(d => d.group === "region")
        : nodes.filter(d => d.group === "university"),
    )
    .enter()
    .append("text")
    .text(d =>
      model.current === 2
        ? toTitle(d.region)
        : toAbbreviateUniversity(d.university),
    )
    .attr("class", "node-text")
    .attr("fill", "#ffffff")
    .attr("text-anchor", "middle")
    .attr("dx", 0)
    .attr("dy", 0);

  const nodeElements = svg
    .select("#nodes")
    .selectAll(".node")
    .data(nodes)
    .enter()
    .append("circle")
    .attr("class", "node")
    .attr("r", d => model.sizeScale(d.size))
    .attr("fill", d => (d.fill ? model.colorScale(d.domain) : "none"));

  nodeElements
    .append("title")
    .text(d =>
      d.group === "expertise-university-domain"
        ? `${toTitle(d.expertise)}\n${d.domain}\n${d.university}\n${
            d.count
          } professeurs`
        : d.group === "university-domain"
        ? `${d.domain}\n${d.university}\n${d.count} professeurs`
        : d.group === "region-domain"
        ? `${d.domain}\n${d.region}\n${d.count} professeurs`
        : "",
    );

  model.simulation.nodes(nodes).on("tick", () => {
    ticked(nodeElements, textElements);
  });

  model.simulation
    .force("link")
    .id(d => d.id)
    .strength(d => d.strength)
    .distance(0)
    .links(links);

  model.simulation
    .alpha(1)
    .alphaTarget(0)
    .restart();
}

function ticked(nodes, nodeTexts) {
  nodes.attr("cx", node => node.x).attr("cy", node => node.y);
  nodeTexts.attr("x", node => node.x).attr("y", node => node.y);
}

function updateSimulation() {
  const svgNode = document.querySelector("#distribution");
  const width = svgNode.getBoundingClientRect().width;
  const height = svgNode.getBoundingClientRect().height;

  if (model.current === 0) {
    model.simulation
      .force("charge", d3.forceManyBody().strength(-100))
      .force(
        "collision",
        d3.forceCollide().radius(d => model.sizeScale(d.size)),
      )
      .force("x", d3.forceX((width * 5) / 9).strength(0.3))
      .force("y", d3.forceY((height * 5) / 9).strength(0.35));
  } else if (model.current === 1) {
    model.simulation
      .force("charge", d3.forceManyBody().strength(-400))
      .force(
        "collision",
        d3.forceCollide().radius(d => model.sizeScale(d.size)),
      )
      .force("x", d3.forceX((width * 5) / 9).strength(0.25))
      .force("y", d3.forceY((height * 5) / 9).strength(0.25));
  } else if (model.current === 2) {
    model.simulation
      .force("charge", d3.forceManyBody().strength(-900))
      .force(
        "collision",
        d3.forceCollide().radius(d => model.sizeScale(d.size)),
      )
      .force("x", d3.forceX((width * 5) / 9).strength(0.3))
      .force("y", d3.forceY((height * 5) / 9).strength(0.3));
  }
}

function getTitle() {
  let title;
  if (model.current === 0) {
    title = "Distribution des expertises par université";
  } else if (model.current === 1) {
    title = "Distribution des domaines par université";
  } else if (model.current === 2) {
    title = "Distribution des domaines par région";
  }
  return `<h1 class="distribution-title">${title}</h1>`;
}

function getExplanation() {
  let explanation;
  if (model.current === 0) {
    explanation =
      "Il y a une plus grande diversité et spécialisation " +
      "d'expertises dans les universités en ville par rapport à celles en " +
      "régions.";
  } else if (model.current === 1) {
    explanation =
      "Certains domaines comme les sciences sociales sont présents dans " +
      "toutes les universités non-spécialisées alors que d'autres commes " +
      "les sciences de la santé sont presque exclusifs à une université.";
  } else {
    explanation =
      "Les sciences (pures, santé et appliquées) se retrouvent presque " +
      "exclusivement en ville. On retrouve aussi la majorité des experts " +
      "en affaires dans la métropole de Montréal.";
  }
  return `<p class="distribution-explanation">${explanation}</p>`;
}

function updateControls() {
  const upPresent = model.current > 0;
  const downPresent = model.current < model.nVis - 1;

  const upControl = d3.select("#up-control");
  const downControl = d3.select("#down-control");

  if (upPresent) {
    upControl.style("display", null);
  } else {
    upControl.style("display", "none");
  }
  if (downPresent) {
    downControl.style("display", null);
  } else {
    downControl.style("display", "none");
  }
}

/********/
/* view */
/********/

function view() {
  graphView();
}

function graphView() {
  createScales();

  legendView();
  controlsView();

  createForceGraph();

  updateNodeAndLinks();
  updateForceGraph();
}

function createForceGraph() {
  const svgNode = document.querySelector("#distribution");
  const height = svgNode.getBoundingClientRect().height;
  const width = svgNode.getBoundingClientRect().width;
  const svg = d3.select(svgNode);

  svg
    .append("g")
    .attr("id", "distribution-title")
    .append("foreignObject")
    .attr("x", width / 2 - 150)
    .attr("y", 10)
    .attr("width", 300)
    .attr("height", 100);

  svg
    .append("g")
    .attr("id", "distribution-explanation")
    .append("foreignObject")
    .attr("width", width / 5 + 20)
    .attr("height", 350)
    .attr("x", 0)
    .attr("y", height - 350);

  svg.append("g").attr("id", "node-texts");

  svg.append("g").attr("id", "nodes");

  const forceLink = d3
    .forceLink()
    .distance(0)
    .strength(d => d.strength);

  model.simulation = d3.forceSimulation().force("link", forceLink);
}

function createScales() {
  const colorScale = d3
    .scaleOrdinal(d3.schemeCategory10)
    .domain([
      ...new Set(
        model.data.filter(d => d.domain !== d.university).map(d => d.domain),
      ),
    ]);

  const sizeScale = d3
    .scaleSqrt()
    .domain([0, model.data.map(d => d.count).reduce((acc, d) => acc + d, 0)])
    .range([
      0,
      2 *
        Math.sqrt(model.data.map(d => d.count).reduce((acc, d) => acc + d, 0)),
    ]);

  [model.colorScale, model.sizeScale] = [colorScale, sizeScale];

  return [colorScale, sizeScale];
}

function legendView() {
  const svgNode = document.querySelector("#distribution");
  const width = svgNode.getBoundingClientRect().width;

  const svg = d3.select(svgNode);

  const sizes = [10, 50, 100];

  const sizeLegend = svg.append("g");

  sizeLegend
    .selectAll("circle")
    .data(sizes)
    .enter()
    .append("circle")
    .attr("cx", width - 130)
    .attr("cy", (_, i) => 50 + i * 65)
    .attr("r", d => model.sizeScale(d))
    .attr("fill", "#d3d3d3");

  sizeLegend
    .selectAll("text")
    .data(sizes)
    .enter()
    .append("text")
    .attr("x", width - 100)
    .attr("y", (_, i) => 55 + i * 65)
    .attr("fill", "white")
    .text(d => `${d} professeurs`);

  const colorLegend = svg.append("g");

  colorLegend
    .selectAll("circle")
    .data(model.colorScale.domain())
    .enter()
    .append("circle")
    .attr("cx", 25)
    .attr("cy", (_, i) => 50 + i * 25)
    .attr("r", d => model.sizeScale(25))
    .attr("fill", d => model.colorScale(d));

  colorLegend
    .selectAll("text")
    .data(model.colorScale.domain())
    .enter()
    .append("text")
    .attr("x", 55)
    .attr("y", (_, i) => 55 + i * 25)
    .attr("fill", "#e9def7")
    .text(d => d);
}

function controlsView() {
  const svgNode = document.querySelector("#distribution");
  const height = svgNode.getBoundingClientRect().height;
  const width = svgNode.getBoundingClientRect().width;

  const svg = d3.select(svgNode);

  const controls = svg.append("g");

  const upControl = controls
    .append("g")
    .attr("id", "up-control")
    .attr("class", "distribution-control")
    .attr("height", 70)
    .attr("width", 70)
    .on("click", () => scrollGraph(false));
  upControl
    .append("circle")
    .attr("cx", width - 100)
    .attr("cy", height - 105)
    .attr("r", 35);
  upControl
    .selectAll("line.up-arrow")
    .data([125, 75])
    .enter()
    .append("line")
    .attr("class", "up-arrow")
    .attr("x1", d => width - d)
    .attr("y1", height - 90)
    .attr("x2", width - 100)
    .attr("y2", height - 120)
    .attr("stroke", "#ffffff")
    .attr("stroke-width", 1);

  const downControl = controls
    .append("g")
    .attr("id", "down-control")
    .attr("class", "distribution-control")
    .attr("height", 70)
    .attr("width", 70)
    .on("click", () => scrollGraph(true));
  downControl
    .append("circle")
    .attr("cx", width - 100)
    .attr("cy", height - 40)
    .attr("r", 35);
  downControl
    .selectAll("line.down-arrow")
    .data([125, 75])
    .enter()
    .append("line")
    .attr("class", "down-arrow")
    .attr("x1", d => width - d)
    .attr("y1", height - 55)
    .attr("x2", width - 100)
    .attr("y2", height - 25)
    .attr("stroke", "#ffffff")
    .attr("stroke-width", 1);
}

/*************/
/* listeners */
/*************/

/*********/
/* utils */
/*********/

/********/
/* init */
/********/

export function init() {
  fetch("data/distribution.csv")
    .then(resp => resp.text())
    .then(data => {
      initModel(data);
      view();
    });
  return scrollGraph;
}
