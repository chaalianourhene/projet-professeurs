"use strict";

export function scroll(id, callback) {}

export function groupBy(data, ...fields) {
  return Object.values(
    data.reduce(
      (grouped, d) =>
        grouped[fields.map(f => d[f])]
          ? {
              ...grouped,
              ...{
                [fields.map(f => d[f])]: {
                  ...grouped[fields.map(f => d[f])],
                  ...{
                    count: grouped[fields.map(f => d[f])].count + d.count || 1,
                  },
                },
              },
            }
          : {
              ...grouped,
              ...{
                [fields.map(f => d[f])]: {
                  ...fields.reduce((a, f) => ({ ...a, ...{ [f]: d[f] } }), {}),
                  ...{ count: d.count || 1 },
                },
              },
            },
      {},
    ),
  );
}

export function toVerboseDomain(domain) {
  const domains = {
    app: "Sciences appliquées",
    art: "Arts",
    aut: "Autre",
    bus: "Affaires",
    dro: "Droit",
    edu: "Éducation",
    let: "Lettres",
    pur: "Sciences pures",
    san: "Sciences santé",
    soc: "Sciences sociales",
  };
  return domains[domain];
}

export function clear(node) {
  while (node.hasChildNodes()) {
    node.removeChild(node.lastChild);
  }
}

export function toTitle(text) {
  return `${text[0].toUpperCase()}${text.slice(1)}`;
}

export function toAbbreviateUniversity(university) {
  const universities = {
    "HEC Montréal": "HEC",
    "Université du Québec à Montréal": "UQÀM",
    "Université du Québec en Outaouais": "UQO",
    "Institut national de la recherche scientifique": "INRS",
    "École de technologie supérieure": "ÉTS",
    "Université TÉLUQ": "TÉLUQ",
    "Bishop university": "Bishop",
    Polytechnique: "Polytechnique", // eslint-disable-line
    "Université Laval": "ULaval",
  };
  return universities[university];
}

export function toRegion(university) {
  const regions = {
    "HEC Montréal": "Montréal",
    "Université du Québec à Montréal": "Montréal",
    "Université du Québec en Outaouais": "Reste du Québec",
    "Institut national de la recherche scientifique": "Montréal",
    "École de technologie supérieure": "Montréal",
    "Université TÉLUQ": "Reste du Québec",
    "Bishop university": "Reste du Québec",
    Polytechnique: "Montréal", // eslint-disable-line
    "Université Laval": "Québec",
  };
  return regions[university];
}
