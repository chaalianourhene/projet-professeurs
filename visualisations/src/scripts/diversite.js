import { toTitle } from "./utils.js";
export function createDiversityGraph() {
  //Select SVG element
  const svg = d3.select("#diversite");
  const svgNode = document.querySelector("#diversite");
  const width = svgNode.getBoundingClientRect().width;
  const height = svgNode.getBoundingClientRect().height;

  function sumEthnicitiesFemale(data) {
    return (
      parseInt(data.Arab_f) +
      parseInt(data.White_f) +
      parseInt(data.Asian_f) +
      parseInt(data.Black_f)
    );
  }

  function sumEthnicitiesMale(data) {
    return (
      parseInt(data.Arab_m) +
      parseInt(data.White_m) +
      parseInt(data.Asian_m) +
      parseInt(data.Black_m)
    );
  }

  function sumEthnicities(data) {
    return sumEthnicitiesMale(data) + sumEthnicitiesFemale(data);
  }

  function allWhite(data) {
    return parseInt(data.White_f) + parseInt(data.White_m);
  }

  function allBlack(data) {
    return parseInt(data.Black_f) + parseInt(data.Black_m);
  }

  function allAsian(data) {
    return parseInt(data.Asian_f) + parseInt(data.Asian_m);
  }

  function allArab(data) {
    return parseInt(data.Arab_f) + parseInt(data.Arab_m);
  }
  /***** Chargement des données *****/
  d3.csv("./data/diversity.csv").then(function(data) {
    const groupedData = d3
      .nest()
      .key(function(d) {
        return d.domain;
      })
      .rollup(function(v) {
        return {
          White_f: d3.sum(v, function(d) {
            return d.White_f;
          }),
          White_m: d3.sum(v, function(d) {
            return d.White_m;
          }),
          Black_f: d3.sum(v, function(d) {
            return d.Black_f;
          }),
          Black_m: d3.sum(v, function(d) {
            return d.Black_m;
          }),
          Asian_f: d3.sum(v, function(d) {
            return d.Asian_f;
          }),
          Asian_m: d3.sum(v, function(d) {
            return d.Asian_m;
          }),
          Arab_f: d3.sum(v, function(d) {
            return d.Arab_f;
          }),
          Arab_m: d3.sum(v, function(d) {
            return d.Arab_m;
          }),
        };
      })
      .entries(data.filter(d => d.university !== "Bishop university"))
      .sort(function(a, b) {
        return a.key.localeCompare(b.key);
      });

    const expertiseDomain = d3
      .nest()
      .key(function(d) {
        return d.domain;
      })
      .entries(data.filter(d => d.university !== "Bishop university"));

    const keys = Object.keys(expertiseDomain);
    let maxheight = 0;
    for (const key of keys) {
      if (expertiseDomain[key].values.length > maxheight) {
        maxheight = expertiseDomain[key].values.length;
      }
    }

    let nbrFlower;
    if (Math.floor((width - 200) / 10 / 35) <= 1) {
      nbrFlower = 1;
    } else {
      nbrFlower = Math.floor((width - 200) / 10 / 35) - 1;
    }

    //svgNode.style.height = (Math.floor(maxheight/nbrFlower)+1)*25+200;
    //height =(Math.floor(maxheight/nbrFlower)+1)*25+200;

    function groupedGraph(groupedData, svg) {
      const nbrDomain = 10;
      const cercleMax = d3.max(groupedData, function(d) {
        return sumEthnicities(d.value);
      });
      const cercleMin = d3.min(groupedData, function(d) {
        return sumEthnicities(d.value);
      });
      const maxR = 10;
      const cercleR = d3
        .scaleSqrt()
        .range([4, maxR])
        .domain([cercleMin, cercleMax]);
      const maxPetale = 25;
      const petaleR = d3
        .scaleSqrt()
        .range([0, maxPetale])
        .domain([0, 100]);
      //dessin des flowers
      const dict = {
        app: "Science Appliquée",
        bus: "Affaires",
        w: "Autre",
        soc: "Science Sociale",
        art: "Art",
        pur: "Science Pure",
        san: "Santé",
        let: "Lettre",
        edu: "Education",
        dro: "Droit",
      };
      const bigflower = svg
        .selectAll(".bigflower")
        .data(groupedData)
        .enter()
        .append("g")
        .attr("class", function(d) {
          return "bigflower " + d.key;
        })
        .on("mouseover", function(d, i) {
          d3.select(this) // eslint-disable-line
            .append("title")
            .text(
              toTitle(dict[d.key]) +
                "\n" +
                " Blanc :" +
                allWhite(d.value) +
                " -" +
                d.value.White_f +
                " f/ " +
                d.value.White_m +
                " m" +
                "\n" +
                " Noir :" +
                allBlack(d.value) +
                " -" +
                d.value.Black_f +
                " f/ " +
                d.value.Black_m +
                " m" +
                "\n" +
                " Arabe :" +
                allArab(d.value) +
                " -" +
                d.value.Arab_f +
                " f/ " +
                d.value.Arab_m +
                " m" +
                "\n" +
                " Asiatique :" +
                allAsian(d.value) +
                " -" +
                d.value.Asian_f +
                " f/ " +
                d.value.Asian_m +
                " m",
            );
        });

      bigflower
        .append("circle")
        .attr("r", function(d) {
          return cercleR(sumEthnicities(d.value));
        })
        .attr("cx", function(d, i) {
          return (
            200 +
            (width - 200) / (nbrDomain * 2) +
            (i * (width - 200)) / nbrDomain
          );
        })
        .attr("cy", function(d, i) {
          return 70;
        })
        .attr("fill", "orange");

      bigflower
        .append("ellipse")
        .attr("ry", function(d) {
          if (d.value.White_m == 0) return 0;
          else
            return petaleR(
              (allWhite(d.value) * 100) / sumEthnicities(d.value),
            );
        })
        .attr("rx", function(d) {
          if (allWhite(d.value) == 0) return 0;
          else return 10;
        })
        .attr("cx", function(d, i) {
          return (
            200 +
            (width - 200) / (nbrDomain * 2) +
            (i * (width - 200)) / nbrDomain
          );
        })
        .attr("cy", function(d, i) {
          return (
            70 -
            maxR -
            petaleR((allWhite(d.value) * 100) / sumEthnicities(d.value))
          );
        })
        .attr("class", "petale-white")
        .style("fill", "grey")
        .attr(
          "transform",
          (d, i) =>
            `rotate(-45, ${200 +
              (width - 200) / (nbrDomain * 2) +
              (i * (width - 200)) / nbrDomain}, ${70})`,
        );
      //.attr("transform", function(d) {return r(-45)});; //rotation not working
      bigflower
        .append("ellipse")
        .attr("ry", function(d) {
          if (d.value.Black_m == 0) return 0;
          else
            return petaleR(
              (allBlack(d.value) * 100) / sumEthnicities(d.value),
            );
        })
        .attr("rx", function(d) {
          if (allBlack(d.value) == 0) return 0;
          else return 10;
        })
        .attr("cx", function(d, i) {
          return (
            200 +
            (width - 200) / (nbrDomain * 2) +
            (i * (width - 200)) / nbrDomain
          );
        })
        .attr("cy", function(d, i) {
          return (
            70 -
            maxR -
            petaleR((allBlack(d.value) * 100) / sumEthnicities(d.value))
          );
        })
        .attr("class", "petale-black")
        .style("fill", "grey")
        .attr(
          "transform",
          (d, i) =>
            `rotate(45, ${200 +
              (width - 200) / (nbrDomain * 2) +
              (i * (width - 200)) / nbrDomain}, ${70})`,
        );

      bigflower
        .append("ellipse")
        .attr("ry", function(d) {
          if (d.value.Asian_m == 0) return 0;
          else
            return petaleR(
              (allAsian(d.value) * 100) / sumEthnicities(d.value),
            );
        })
        .attr("rx", function(d) {
          if (allAsian(d.value) == 0) return 0;
          else return 10;
        })
        .attr("cx", function(d, i) {
          return (
            200 +
            (width - 200) / (nbrDomain * 2) +
            (i * (width - 200)) / nbrDomain
          );
        })
        .attr("cy", function(d, i) {
          return (
            70 -
            maxR -
            petaleR((allAsian(d.value) * 100) / sumEthnicities(d.value))
          );
        })
        .attr("class", "petale-asian")
        .style("fill", "grey")
        .attr(
          "transform",
          (d, i) =>
            `rotate(135, ${200 +
              (width - 200) / (nbrDomain * 2) +
              (i * (width - 200)) / nbrDomain}, ${70})`,
        );

      bigflower
        .append("ellipse")
        .attr("ry", function(d) {
          if (d.value.Arab_m == 0) return 0;
          else
            return petaleR((allArab(d.value) * 100) / sumEthnicities(d.value));
        })
        .attr("rx", function(d) {
          if (allArab(d.value) == 0) return 0;
          else return 10;
        })
        .attr("cx", function(d, i) {
          return (
            200 +
            (width - 200) / (nbrDomain * 2) +
            (i * (width - 200)) / nbrDomain
          );
        })
        .attr("cy", function(d, i) {
          return (
            70 -
            maxR -
            petaleR((allArab(d.value) * 100) / sumEthnicities(d.value))
          );
        })
        .attr("class", "petale-arab")
        .style("fill", "grey")
        .attr(
          "transform",
          (d, i) =>
            `rotate(-135, ${200 +
              (width - 200) / (nbrDomain * 2) +
              (i * (width - 200)) / nbrDomain}, ${70})`,
        );

      bigflower
        .append("text")
        .attr("x", function(d, i) {
          return (
            195 +
            (width - 200) / (nbrDomain * 2) +
            (i * (width - 200)) / nbrDomain -
            dict[d.key].length * 2.3
          );
        })

        .attr("y", 70 + maxR + maxPetale + 10)
        .text(function(d) {
          return dict[d.key];
        })
        .attr("font-family", "sans-serif")
        .attr("font-size", "11px")
        //.attr("font-weight","bold")
        .attr("fill", "rgb(245,245,245)");

      bigflower
        .append("rect")
        .attr("x", function(d, i) {
          return 200 + (i * (width - 200)) / 10;
        })
        .attr("y", 80)
        .attr("width", 1)
        .attr("height", height)
        .attr("fill", "rgb(245,245,245)");
    }

    groupedGraph(groupedData, svg);

    function elementGraph(data, svg, nbrFlower) {
      const cercleMax = d3.max(data, function(d) {
        return sumEthnicities(d);
      });
      const cercleMin = d3.min(data, function(d) {
        return sumEthnicities(d);
      });

      const cercleR = d3
        .scaleSqrt()
        .range([1, 3.5])
        .domain([cercleMin, cercleMax]);
      const petaleR = d3
        .scaleSqrt()
        .range([0, 10])
        .domain([0, 100]);

      const dict = {
        app: 0,
        bus: 2,
        w: 9,
        soc: 8,
        art: 1,
        pur: 6,
        san: 7,
        let: 5,
        edu: 4,
        dro: 3,
      };
      const flower = svg
        .selectAll(".bigflower")
        .selectAll(".flower")
        .data(function(i) {
          return data.filter(function(d) {
            return d.domain === i.key;
          });
        })
        .enter()
        .append("g")
        .attr("class", function(d) {
          return "flower " + d.expertise;
        })
        .on("mouseover", function(d, i) {
          d3.select(this) // eslint-disable-line
            .append("title")
            .text(
              toTitle(d.expertise) +
                "\n" +
                " Blanc :" +
                allWhite(d) +
                " -" +
                d.White_f +
                " f/ " +
                d.White_m +
                " m" +
                "\n" +
                " Noir :" +
                allBlack(d) +
                " -" +
                d.Black_f +
                " f/ " +
                d.Black_m +
                " m" +
                "\n" +
                " Arabe :" +
                allArab(d) +
                " -" +
                d.Arab_f +
                " f/ " +
                d.Arab_m +
                " m" +
                "\n" +
                " Asiatique :" +
                allAsian(d) +
                " -" +
                d.Asian_f +
                " f/ " +
                d.Asian_m +
                " m",
            )
            .attr(
              "x",
              200 +
                (dict[d.domain] * (width - 200)) / 10 +
                (i % nbrFlower) * 25 +
                25,
            )
            .attr("y", Math.floor(i / nbrFlower) * 40 + 150);
        });

      flower
        .append("circle")
        .attr("r", function(d) {
          return cercleR(sumEthnicities(d));
        })
        .attr("cx", function(d, i) {
          return (
            200 +
            (dict[d.domain] * (width - 200)) / 10 +
            ((i % nbrFlower) * ((width - 200) / 10 + 10)) / nbrFlower +
            25
          );
        })
        .attr("cy", function(d, i) {
          return Math.floor(i / nbrFlower) * 40 + 150;
        })
        .attr("fill", "orange");

      /***** dessin des petales *****/
      flower
        .append("ellipse")
        .attr("ry", function(d) {
          if (d.White_m == 0) return 0;
          else return petaleR((allWhite(d) * 100) / sumEthnicities(d));
        })
        .attr("rx", function(d) {
          if (allWhite(d) == 0) return 0;
          else return 5;
        })
        .attr("cx", function(d, i) {
          return (
            200 +
            (dict[d.domain] * (width - 200)) / 10 +
            ((i % nbrFlower) * ((width - 200) / 10 + 10)) / nbrFlower +
            25
          );
        })
        .attr("cy", function(d, i) {
          return (
            Math.floor(i / nbrFlower) * 40 +
            150 -
            3.5 -
            petaleR((allWhite(d) * 100) / sumEthnicities(d))
          );
        })
        .attr("class", "petale-white")
        .style("fill", "grey")
        .attr(
          "transform",
          (d, i) =>
            `rotate(-45, ${200 +
              (dict[d.domain] * (width - 200)) / 10 +
              ((i % nbrFlower) * ((width - 200) / 10 + 10)) / nbrFlower +
              25}, ${Math.floor(i / nbrFlower) * 40 + 150})`,
        );
      //.attr("transform", function(d) {return r(-45)});; //rotation not working
      flower
        .append("ellipse")
        .attr("ry", function(d) {
          if (d.Black_m == 0) return 0;
          else return petaleR((allBlack(d) * 100) / sumEthnicities(d));
        })
        .attr("rx", function(d) {
          if (allBlack(d) == 0) return 0;
          else return 5;
        })
        .attr("cx", function(d, i) {
          return (
            200 +
            (dict[d.domain] * (width - 200)) / 10 +
            ((i % nbrFlower) * ((width - 200) / 10 + 10)) / nbrFlower +
            25
          );
        })
        .attr("cy", function(d, i) {
          return (
            Math.floor(i / nbrFlower) * 40 +
            150 -
            3.5 -
            petaleR((allBlack(d) * 100) / sumEthnicities(d))
          );
        })
        .attr("class", "petale-black")
        .style("fill", "grey")
        .attr(
          "transform",
          (d, i) =>
            `rotate(45, ${200 +
              (dict[d.domain] * (width - 200)) / 10 +
              ((i % nbrFlower) * ((width - 200) / 10 + 10)) / nbrFlower +
              25}, ${Math.floor(i / nbrFlower) * 40 + 150})`,
        );

      flower
        .append("ellipse")
        .attr("ry", function(d) {
          if (d.Asian_m == 0) return 0;
          else return petaleR((allAsian(d) * 100) / sumEthnicities(d));
        })
        .attr("rx", function(d) {
          if (allAsian(d) == 0) return 0;
          else return 5;
        })
        .attr("cx", function(d, i) {
          return (
            200 +
            (dict[d.domain] * (width - 200)) / 10 +
            ((i % nbrFlower) * ((width - 200) / 10 + 10)) / nbrFlower +
            25
          );
        })
        .attr("cy", function(d, i) {
          return (
            Math.floor(i / nbrFlower) * 40 +
            150 -
            3.5 -
            petaleR((allAsian(d) * 100) / sumEthnicities(d))
          );
        })
        .attr("class", "petale-asian")
        .style("fill", "grey")
        .attr(
          "transform",
          (d, i) =>
            `rotate(135, ${200 +
              (dict[d.domain] * (width - 200)) / 10 +
              ((i % nbrFlower) * ((width - 200) / 10 + 10)) / nbrFlower +
              25}, ${Math.floor(i / nbrFlower) * 40 + 150})`,
        );

      flower
        .append("ellipse")
        .attr("ry", function(d) {
          if (d.Arab_m == 0) return 0;
          else return petaleR((allArab(d) * 100) / sumEthnicities(d));
        })
        .attr("rx", function(d) {
          if (allArab(d) == 0) return 0;
          else return 5;
        })
        .attr("cx", function(d, i) {
          return (
            200 +
            (dict[d.domain] * (width - 200)) / 10 +
            ((i % nbrFlower) * ((width - 200) / 10 + 10)) / nbrFlower +
            25
          );
        })
        .attr("cy", function(d, i) {
          return (
            Math.floor(i / nbrFlower) * 40 +
            150 -
            3.5 -
            petaleR((allArab(d) * 100) / sumEthnicities(d))
          );
        })
        .attr("class", "petale-arab")
        .style("fill", "grey")
        .attr(
          "transform",
          (d, i) =>
            `rotate(-135, ${200 +
              (dict[d.domain] * (width - 200)) / 10 +
              ((i % nbrFlower) * ((width - 200) / 10 + 10)) / nbrFlower +
              25}, ${Math.floor(i / nbrFlower) * 40 + 150})`,
        );
    }
    elementGraph(data, svg, nbrFlower);

    function drawGraphFemaleGrouped(groupedData, svg) {
      const nbrDomain = 10;
      const cercleMax = d3.max(groupedData, function(d) {
        return sumEthnicities(d.value);
      });
      const cercleMin = d3.min(groupedData, function(d) {
        return sumEthnicities(d.value);
      });
      const maxR = 10;
      d3.scaleSqrt()
        .range([4, maxR])
        .domain([cercleMin, cercleMax]);
      const maxPetale = 20;
      const petaleR = d3
        .scaleSqrt()
        .range([0, maxPetale])
        .domain([0, 100]);
      //dessin des flowers
      const bigflower = svg.selectAll(".bigflower").data(groupedData);

      bigflower
        .append("ellipse")
        .attr("ry", function(d) {
          return petaleR((d.value.White_f * 100) / sumEthnicities(d.value));
        })
        .attr("rx", function(d) {
          if (d.value.White_f == 0) return 0;
          else return 8;
        })
        .attr("cx", function(d, i) {
          return (
            200 +
            (width - 200) / (nbrDomain * 2) +
            (i * (width - 200)) / nbrDomain
          );
        })
        .attr("cy", function(d, i) {
          return (
            70 -
            maxR -
            petaleR((d.value.White_f * 100) / sumEthnicities(d.value))
          );
        })
        .attr("class", "petale-white")
        .style("fill", "salmon")
        .attr(
          "transform",
          (d, i) =>
            `rotate(-45, ${200 +
              (width - 200) / (nbrDomain * 2) +
              (i * (width - 200)) / nbrDomain}, ${70})`,
        );
      //.attr("transform", function(d) {return r(-45)});; //rotation not working
      bigflower
        .append("ellipse")
        .attr("ry", function(d) {
          return petaleR((d.value.Black_f * 100) / sumEthnicities(d.value));
        })
        .attr("rx", function(d) {
          if (d.value.Black_f == 0) return 0;
          else return 8;
        })
        .attr("cx", function(d, i) {
          return (
            200 +
            (width - 200) / (nbrDomain * 2) +
            (i * (width - 200)) / nbrDomain
          );
        })
        .attr("cy", function(d, i) {
          return (
            70 -
            maxR -
            petaleR((d.value.Black_f * 100) / sumEthnicities(d.value))
          );
        })
        .attr("class", "petale-black")
        .style("fill", "salmon")
        .attr(
          "transform",
          (d, i) =>
            `rotate(45, ${200 +
              (width - 200) / (nbrDomain * 2) +
              (i * (width - 200)) / nbrDomain}, ${70})`,
        );

      bigflower
        .append("ellipse")
        .attr("ry", function(d) {
          return petaleR((d.value.Asian_f * 100) / sumEthnicities(d.value));
        })
        .attr("rx", function(d) {
          if (d.value.Asian_f == 0) return 0;
          else return 8;
        })
        .attr("cx", function(d, i) {
          return (
            200 +
            (width - 200) / (nbrDomain * 2) +
            (i * (width - 200)) / nbrDomain
          );
        })
        .attr("cy", function(d, i) {
          return (
            70 -
            maxR -
            petaleR((d.value.Asian_f * 100) / sumEthnicities(d.value))
          );
        })
        .attr("class", "petale-asian")
        .style("fill", "salmon")
        .attr(
          "transform",
          (d, i) =>
            `rotate(135, ${200 +
              (width - 200) / (nbrDomain * 2) +
              (i * (width - 200)) / nbrDomain}, ${70})`,
        );

      bigflower
        .append("ellipse")
        .attr("ry", function(d) {
          return petaleR((d.value.Arab_f * 100) / sumEthnicities(d.value));
        })
        .attr("rx", function(d) {
          if (d.value.Arab_f == 0) return 0;
          else return 8;
        })
        .attr("cx", function(d, i) {
          return (
            200 +
            (width - 200) / (nbrDomain * 2) +
            (i * (width - 200)) / nbrDomain
          );
        })
        .attr("cy", function(d, i) {
          return (
            70 -
            maxR -
            petaleR((d.value.Arab_f * 100) / sumEthnicities(d.value))
          );
        })
        .attr("class", "petale-arab")
        .style("fill", "salmon")
        .attr(
          "transform",
          (d, i) =>
            `rotate(-135, ${200 +
              (width - 200) / (nbrDomain * 2) +
              (i * (width - 200)) / nbrDomain}, ${70})`,
        );
    }
    drawGraphFemaleGrouped(groupedData, svg);

    function elementGraphFemale(data, svg, nbrFlower) {
      const cercleMax = d3.max(data, function(d) {
        return sumEthnicities(d);
      });
      const cercleMin = d3.min(data, function(d) {
        return sumEthnicities(d);
      });

      d3.scaleSqrt()
        .range([1, 3.5])
        .domain([cercleMin, cercleMax]);
      const petaleR = d3
        .scaleSqrt()
        .range([0, 8])
        .domain([0, 100]);

      const dict = {
        app: 0,
        bus: 2,
        w: 9,
        soc: 8,
        art: 1,
        pur: 6,
        san: 7,
        let: 5,
        edu: 4,
        dro: 3,
      };
      const flower = svg
        .selectAll(".bigflower")
        .selectAll(".flower")
        .data(function(i) {
          return data.filter(function(d) {
            return d.domain === i.key;
          });
        });

      /***** dessin des petales *****/
      flower
        .append("ellipse")
        .attr("ry", function(d) {
          return petaleR((d.White_f * 100) / sumEthnicities(d));
        })
        .attr("rx", function(d) {
          if (d.White_f == 0) return 0;
          else return 4;
        })
        .attr("cx", function(d, i) {
          return (
            200 +
            (dict[d.domain] * (width - 200)) / 10 +
            ((i % nbrFlower) * ((width - 200) / 10 + 10)) / nbrFlower +
            25
          );
        })
        .attr("cy", function(d, i) {
          return (
            Math.floor(i / nbrFlower) * 40 +
            150 -
            3.5 -
            petaleR((d.White_f * 100) / sumEthnicities(d))
          );
        })
        .attr("class", "petale-white")
        .style("fill", "salmon")
        .attr(
          "transform",
          (d, i) =>
            `rotate(-45, ${200 +
              (dict[d.domain] * (width - 200)) / 10 +
              ((i % nbrFlower) * ((width - 200) / 10 + 10)) / nbrFlower +
              25}, ${Math.floor(i / nbrFlower) * 40 + 150})`,
        );
      //.attr("transform", function(d) {return r(-45)});; //rotation not working
      flower
        .append("ellipse")
        .attr("ry", function(d) {
          return petaleR((d.Black_f * 100) / sumEthnicities(d));
        })
        .attr("rx", function(d) {
          if (d.Black_f == 0) return 0;
          else return 4;
        })
        .attr("cx", function(d, i) {
          return (
            200 +
            (dict[d.domain] * (width - 200)) / 10 +
            ((i % nbrFlower) * ((width - 200) / 10 + 10)) / nbrFlower +
            25
          );
        })
        .attr("cy", function(d, i) {
          return (
            Math.floor(i / nbrFlower) * 40 +
            150 -
            3.5 -
            petaleR((d.Black_f * 100) / sumEthnicities(d))
          );
        })
        .attr("class", "petale-black")
        .style("fill", "salmon")
        .attr(
          "transform",
          (d, i) =>
            `rotate(45, ${200 +
              (dict[d.domain] * (width - 200)) / 10 +
              ((i % nbrFlower) * ((width - 200) / 10 + 10)) / nbrFlower +
              25}, ${Math.floor(i / nbrFlower) * 40 + 150})`,
        );

      flower
        .append("ellipse")
        .attr("ry", function(d) {
          return petaleR((d.Asian_f * 100) / sumEthnicities(d));
        })
        .attr("rx", function(d) {
          if (d.Asian_f == 0) return 0;
          else return 4;
        })
        .attr("cx", function(d, i) {
          return (
            200 +
            (dict[d.domain] * (width - 200)) / 10 +
            ((i % nbrFlower) * ((width - 200) / 10 + 10)) / nbrFlower +
            25
          );
        })
        .attr("cy", function(d, i) {
          return (
            Math.floor(i / nbrFlower) * 40 +
            150 -
            3.5 -
            petaleR((d.Asian_f * 100) / sumEthnicities(d))
          );
        })
        .attr("class", "petale-asian")
        .style("fill", "salmon")
        .attr(
          "transform",
          (d, i) =>
            `rotate(135, ${200 +
              (dict[d.domain] * (width - 200)) / 10 +
              ((i % nbrFlower) * ((width - 200) / 10 + 10)) / nbrFlower +
              25}, ${Math.floor(i / nbrFlower) * 40 + 150})`,
        );

      flower
        .append("ellipse")
        .attr("ry", function(d) {
          return petaleR((d.Arab_f * 100) / sumEthnicities(d));
        })
        .attr("rx", function(d) {
          if (d.Arab_f == 0) return 0;
          else return 4;
        })
        .attr("cx", function(d, i) {
          return (
            200 +
            (dict[d.domain] * (width - 200)) / 10 +
            ((i % nbrFlower) * ((width - 200) / 10 + 10)) / nbrFlower +
            25
          );
        })
        .attr("cy", function(d, i) {
          return (
            Math.floor(i / nbrFlower) * 40 +
            150 -
            3.5 -
            petaleR((d.Arab_f * 100) / sumEthnicities(d))
          );
        })
        .attr("class", "petale-arab")
        .style("fill", "salmon")
        .attr(
          "transform",
          (d, i) =>
            `rotate(-135, ${200 +
              (dict[d.domain] * (width - 200)) / 10 +
              ((i % nbrFlower) * ((width - 200) / 10 + 10)) / nbrFlower +
              25}, ${Math.floor(i / nbrFlower) * 40 + 150})`,
        );
    }
    elementGraphFemale(data, svg, nbrFlower);
  });

  //Etiquette
  function etiquette(svg) {
    //Draw line
    svg
      .append("rect")
      .attr("x", 200)
      .attr("y", 0)
      .attr("width", 2.5)
      .attr("height", height + 80)
      .attr("fill", "rgb(245,245,245)");

    //add title How to read this:
    svg
      .append("text")
      .attr("x", 25)
      .attr("y", 30)
      .text("Comment Le Lire")
      .attr("font-family", "sans-serif")
      .attr("font-size", "20px")
      //.attr("font-weight","bold")
      .attr("fill", "rgb(245,245,245)");

    svg
      .append("text")
      .attr("x", 20)
      .attr("y", 70)
      .text("Un pétale")
      .attr("font-family", "sans-serif")
      .attr("font-size", "15px")
      .attr("font-weight", "bold")
      .attr("fill", "rgb(245,245,245)");

    svg
      .append("text")
      .attr("x", 100)
      .attr("y", 70)
      .text("représente")
      .attr("font-family", "sans-serif")
      .attr("font-size", "15px")
      //.attr("font-weight","bold")
      .attr("fill", "rgb(245,245,245)");

    svg
      .append("text")
      .attr("x", 20)
      .attr("y", 90)
      .text("une catégorie ethnique")
      .attr("font-family", "sans-serif")
      .attr("font-size", "15px")
      //.attr("font-weight","bold")
      .attr("fill", "rgb(245,245,245)");

    //Flower to read

    svg
      .append("circle")
      .attr("cx", 100)
      .attr("cy", 230)
      .attr("r", 15)
      .attr("fill", "orange");

    svg
      .append("ellipse")
      .attr("cx", 100)
      .attr("cy", 230 - 70)
      .attr("rx", 25)
      .attr("ry", 50)
      .attr("fill", "gray")
      .attr("transform", "rotate(-45, 100, 230)");

    svg
      .append("text")
      .attr("x", 5)
      .attr("y", 130)
      .text("Blanc")
      .attr("font-family", "sans-serif")
      .attr("font-size", "15px")
      //.attr("font-weight","bold")
      .attr("fill", "rgb(245,245,245)");

    svg
      .append("ellipse")
      .attr("cx", 100)
      .attr("cy", 230 - 70)
      .attr("rx", 25)
      .attr("ry", 50)
      .attr("fill", "gray")
      .attr("transform", "rotate(45, 100, 230)");

    svg
      .append("text")
      .attr("x", 160)
      .attr("y", 130)
      .text("Noir")
      .attr("font-family", "sans-serif")
      .attr("font-size", "15px")
      //.attr("font-weight","bold")
      .attr("fill", "rgb(245,245,245)");

    svg
      .append("ellipse")
      .attr("cx", 100)
      .attr("cy", 230 - 70)
      .attr("rx", 25)
      .attr("ry", 50)
      .attr("fill", "gray")
      .attr("transform", "rotate(-135, 100, 230)");

    svg
      .append("text")
      .attr("x", 5)
      .attr("y", 340)
      .text("Arabe")
      .attr("font-family", "sans-serif")
      .attr("font-size", "15px")
      //.attr("font-weight","bold")
      .attr("fill", "rgb(245,245,245)");

    svg
      .append("ellipse")
      .attr("cx", 100)
      .attr("cy", 230 - 70)
      .attr("rx", 25)
      .attr("ry", 50)
      .attr("fill", "gray")
      .attr("transform", "rotate(135, 100, 230)");

    svg
      .append("text")
      .attr("x", 130)
      .attr("y", 340)
      .text("Asiatique")
      .attr("font-family", "sans-serif")
      .attr("font-size", "15px")
      .attr("fill", "rgb(245,245,245)");

    svg
      .append("rect")
      .attr("x", 100)
      .attr("y", 230)
      .attr("height", 120)
      .attr("width", 1)
      .attr("fill", "rgb(245,245,245)");

    svg
      .append("text")
      .attr("x", 30)
      .attr("y", 230 + 140)
      .text("Total des #Professeurs")
      .attr("font-family", "sans-serif")
      .attr("font-size", "15px")
      //.attr("font-weight","bold")
      .attr("fill", "rgb(245,245,245)");

    svg
      .append("rect")
      .attr("x", 25)
      .attr("y", 230 + 170)
      .attr("width", 20)
      .attr("height", 20)
      .attr("fill", "grey");

    svg
      .append("rect")
      .attr("x", 25)
      .attr("y", 230 + 170 + 50)
      .attr("width", 20)
      .attr("height", 20)
      .attr("fill", "salmon");
    svg
      .append("text")
      .attr("x", 25 + 20 + 15)
      .attr("y", 230 + 170 + 15)
      .text("Homme")
      .attr("font-family", "sans-serif")
      .attr("font-size", "15px")
      //.attr("font-weight","bold")
      .attr("fill", "rgb(245,245,245)");
    svg
      .append("text")
      .attr("x", 25 + 20 + 15)
      .attr("y", 230 + 170 + 50 + 15)
      .text("Femme")
      .attr("font-family", "sans-serif")
      .attr("font-size", "15px")
      //.attr("font-weight","bold")
      .attr("fill", "rgb(245,245,245)");
  }
  etiquette(svg);
}
