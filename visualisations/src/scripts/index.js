"use strict";
import * as distribution from "./distribution.js";
import { createDiversityGraph } from "./diversite.js";

/*********/
/* model */
/*********/

let model;

function initModel() {
  model = {
    section:
      window.pageYOffset <=
      document.querySelector("#visualisations").getBoundingClientRect().top
        ? null
        : "visualisation",
    selected: "diversity",
    scrollFunctions: {
      diversite: null,
      distribution: null,
    },
    scrolling: false,
  };
}

/********/
/* view */
/********/

function view() {
  visualisationView();
}

function visualisationView(scrollTo = false) {
  if (model.selected === "diversity") {
    document.querySelectorAll(".switch-button--background").forEach(elem => {
      elem.style.left = "-1px";
    });
    document.querySelector("#diversite").style.transform = "translateX(0)";
    document.querySelector("#distribution").style.transform =
      "translateX(100vw)";
  } else {
    document.querySelectorAll(".switch-button--background").forEach(elem => {
      elem.style.left = "149px";
    });
    document.querySelector("#diversite").style.transform =
      "translateX(-100vw)";
    document.querySelector("#distribution").style.transform = "translateX(0)";
  }
  if (scrollTo) {
    scroll(document.querySelector("#visualisation-section"));
  }
}

function scroll(element) {
  model.scrolling = true;
  element.scrollIntoView({ behavior: "smooth", block: "start" });
  setTimeout(() => {
    model.scrolling = false;
  }, 100);
}

/**********/
/* update */
/**********/

function update() {
  updateSection();
}

function toggleVisualisation() {
  if (model.section == "visualisation") {
    if (model.selected === "diversity") {
      model.selected = "distribution";
    } else {
      model.selected = "diversity";
    }
  }
  visualisationView(true);
}

function handleScroll(event) {
  if (model.section === "visualisation") {
    let down;
    if (event.type === "keydown") {
      if (event.key === "ArrowDown") {
        down = true;
      } else if (event.key === "ArrowUp") {
        down = false;
      } else if (
        (event.key === "ArrowLeft" && model.selected === "distribution") ||
        (event.key === "ArrowRight" && model.selected === "diversity")
      ) {
        toggleVisualisation();
      } else {
        return true;
      }
    } else {
      down = event.deltaY > 0;
    }

    let graphScrolled;
    if (model.selected === "distribution") {
      graphScrolled = model.scrollFunctions.distribution(down);
    } else {
      graphScrolled = false;
    }
    if (!graphScrolled) {
      return;
    } else {
      event.preventDefault();
    }
  }
}

function updateSection() {
  const visualisationPosition = document.querySelector(
    "#visualisation-section",
  ).offsetTop;
  const conclusionPosition = document.querySelector("#conclusion").offsetTop;
  if (
    (window.pageYOffset >= visualisationPosition &&
      window.pageYOffset < conclusionPosition) ||
    visualisationPosition === null
  ) {
    model.section = "visualisation";
  } else {
    model.section = null;
  }
}

/*************/
/* listeners */
/*************/

function addEventListeners() {
  addSwitchButtonListeners();
  addScrollEventListeners();
}

function addSwitchButtonListeners() {
  document.querySelectorAll(".switch-button").forEach(elem => {
    elem.addEventListener("click", () => toggleVisualisation());
  });
}

function addScrollEventListeners() {
  window.addEventListener("wheel", event => handleScroll(event));
  window.addEventListener("keydown", event => handleScroll(event));
  window.addEventListener("scroll", event => updateSection());
}

/********/
/* init */
/********/

function init() {
  initModel();
  update();
  view();
  addEventListeners();

  model.scrollFunctions.distribution = distribution.init();
  createDiversityGraph();
}

window.onload = init;
